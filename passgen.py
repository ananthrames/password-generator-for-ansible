#!/usr/bin/python3

import subprocess
import secrets
import string
import os
import fileinput

deci = int(input("Need to genereate key passphrase? yes[1]/no[0]"))
quo = "'"
subprocess.call(["sed", "-i", '/^[[:space:]]*$/d', "users.txt"])


def withkey():
    def get_secure_random_string(length):
        secure_str = ''.join((secrets.choice(
            string.ascii_letters + string.digits + "$" + "&" + "!" + "#" + "@" + "%" + "^" + "*" + "~" + "`" + "=" + "+" + "-" + "|/|" + "|\|" + "<" + ">" + "_" + ":" + ";" + ",")
                              for i in range(length)))

        return secure_str
    users_array = []
    with open('users.txt') as usersfile:
        for line in usersfile:
            users_array.append(line)
        print("userlist:")
        for x in range(len(users_array)):
            print(" ", "-" + " " + "username:", users_array[x], end='')
            n1 = get_secure_random_string(16)
            print(" ", " ", "password:", quo + n1 + quo)
            k1 = get_secure_random_string(16)
            print(" ", " ", "passphrase:", quo + k1 + quo)


def withoutkey():
    def get_secure_random_string(length):
        secure_str = ''.join((secrets.choice(
            string.ascii_letters + string.digits + "$" + "&" + "!" + "#" + "@" + "%" + "^" + "*" + "~" + "`" + "=" + "+" + "-" + "|/|" + "|\|" + "<" + ">" + "_" + ":" + ";" + ",")
                              for i in range(length)))
        return secure_str

    users_array = []
    with open('users.txt') as usersfile:
        for line in usersfile:
            users_array.append(line)
    print("userlist:")
    for x in range(len(users_array)):
        print(" ", "-" + " " + "username:", users_array[x], end='')
        n1 = get_secure_random_string(16)
        print(" ", " ", "password:", quo + n1 + quo)


if deci == 0:
    withoutkey()
elif deci == 1:
    withkey()
else:
    print("Invalid Option")
